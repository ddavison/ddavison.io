---
layout: post
title: "Thoughts on advertisements"
description: ""
cover: cover.jpg
category: 
tags: [google, ads, advertisements]
---

I'll be honest - I've never enjoyed advertisements.  That's probably pretty obvious, but from an ethical perspective,
I find advertisements exploitative and poisonous.  I feel that ads appeal mostly to people that can't think for themselves,
or they are designed in such a way to be clicked inadvertently. 

As a person in software, I've always had the opportunity to invest in ad-space.  By paying an ad provider ~$100USD and 
utilizing some ad API I could subject you, the reader, to ads on your left, right top and bottom of this very page. 
It would be super easy and cheap, but I refuse to do so.  

You might be wondering to yourself, "Umm... Why not just sell-out?  You could make money off of your site!"  Some people
might consider me stupid for not doing so, but ultimately it is what I personally feel is **right** and **ethical**.
I refuse to sell out to any company, entity or person to further my own gains.  Specifically, monetary gain. 
In so doing, I feel that you are subjecting your audience to content that wasn't curated by you.

Historically this sort of thing happened all of the time on television.  I recall watching a TV show on one channel, and
getting an advertisement for a competing TV channel.

The greatest example of this would be my recent experience using search engines.

## Search Engines

Search engines and providers have evolved a LOT since I've been involved with technology (circa 2009.)  When you'd like 
to know something, or look up something - enter your search term, hit enter and there you go.  Results were sorted by 
all sorts of algorithms, including but not limited to...

- The date that the content was made available.
- Popularity of search. (If people were searching for the same term and clicking a result, that result would be "nudged"
  so to speak)
- Any search operators you might have included in your search.

More likely than not, the first result that was returned was the one you wanted.

I don't think it's news to say that Google is no longer a search engine.  Google, today is an advertisement provider.

Today - You cannot help look for something like: `gitlab feature flags` and get at least two ads subjected to you
that likely have no relevancy to a term you have put in.

I've boxed the result that was desired in red.

### Google

*Searched for `gitlab feature flags` but my result was below two ads **completely** irrelevant to "gitlab"*

<img alt="google ads" src="/images/thoughts-on-advertisements/google-ads.png" width="938" height="699" />

### Bing

*Searched for `wazeapp` but my result was number 5 in the list...*

<img alt="bing ads" src="/images/thoughts-on-advertisements/bing-ads.png" width="500" height="633" />

### DuckDuckGo

Even DuckDuckGo has fallen into the privy of exploitation.

*Searched for `gitlab feature flags`. My result was buried under an advertisement, again - unrelated to gitlab*

<img alt="ddg.gg ads" src="/images/thoughts-on-advertisements/duckduckgo-ads.png" width="500" height="429" />

## Reddit

I joined Reddit in June of 2012.  This year I celebrated my 7th cake day.  I've always found Reddit to be a place that 
could only really be described by the word "Free internet."  Anybody could post anything.

Recently I was browsing through Reddit and realized how **dirty** these ads can be.

<img src="/images/thoughts-on-advertisements/reddit-ads.png" width="367" height="712" />

This ad is almost indistinguishable from a regular Reddit post.  Cleverly, reddit even puts the upvote/downvote there.

Suffice to say, this particular advertisement style is very effective, yet very exploitative.

After finding that reddit executes this sort of dirty game by putting ads in the middle of posts, in the future I will 
be more careful.

### Aaron Swartz

If you do not know about [Aaron Swartz](https://en.wikipedia.org/wiki/Aaron_Swartz), I would strongly recommend you read
up on him.

The reason I bring Aaron up in this article is because of his activism in freedom of information.  Reddit today is 
"yet another company" that subjects its users to unsolicited advertising, which I believe hinders said freedom of 
information.  Advertisements distract and occupy, rather than give you solid information.

## Not all ads are bad...

I can give a few examples of what I would appropriate ads.

- Rewards-based ads
  * These ads would be rendered to the user as a pro-quo type situation.  I will watch your ad if you give me something 
    in return.  Often times you will find these in mobile games.
- Non-intrusive ads
  * The best example of a non-intrusive ad I can think of would be [Waze](https://waze.com/).  When you are routing to
    your destination, when you happen upon a stop sign, Waze will load an advertisement of a nearby restaurant.  I would
    consider this a non-intrusive ad since I can still see the route.  Additionally, the advertisement will disappear 
    when you resume your drive.
- Ads not in the virtual realm.
  * A radio station for instance gives advertisements.  This is so the individuals running the station can step out to
    eat lunch, use the restroom - whatever is necessary.  Totally reasonable.
  * Billboards are intrusive, but I think they are instrumental for spreading awareness of new companies that might be
    sprouting up around the area.  **Today**, however I think the billboard space is run by bidding and no Joe Shmoe can
    outbid a bigger company.
