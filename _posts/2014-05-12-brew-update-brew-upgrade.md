---
layout: post
title: "Upgrade to Mavericks"
description: ""
cover: cover.jpg
category: 
tags: [osx,brew]
---

Recently, I had upgraded my machine from version 10.8.5 to 10.9.2 (Mavericks) and **immediately** noticed some issues with it.

Notably, many of my binaries were no longer working that I had installed with [Homebrew](https://brew.sh).

If you see these issues, you can fix it in one command...

```sh
brew update && brew upgrade
```

Once you do this, your binaries linked with `brew` should now work again! (hopefully..)
