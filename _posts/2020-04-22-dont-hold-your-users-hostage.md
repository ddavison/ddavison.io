---
layout: post
title: "Don't hold your users hostage"
description: "Let's start presenting pertinent information to the user the right way"
cover: cover.jpg
category: ramblings
tags: [rants, metrics, ux]
---

One of the biggest pitfalls with UX and metrics, is collecting information at the _expense_ of the end user.

When I am browsing a site and a [modal](https://en.wikipedia.org/wiki/Modal_window) pops up, requiring action from me -
you've taken me hostage.  I now must click a button to close said modal.

You've impeded my experience with the site at the expense of collecting metrics.  We invented 
[Cookies](https://en.wikipedia.org/wiki/HTTP_cookie) so we could store temporary data on an end-user's browser. 

Let's use those cookies to store a `UserSawMessage=true` on rendering of a message and if we need to collect that,
go for it. By using that cookie, you can also program your site not to show that "survey", broadcasted message, or
modal. Don't force the user to interact with said component.

You shouldn't take your end-users hostage in order to collect metrics.  No amount of data collected could ever truly
represent your user base. 

See [this site](/metrics) that I devised to showcase why.
